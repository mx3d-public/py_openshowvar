"""
A Python port of KUKA VarProxy client (OpenShowVar).
"""

import asyncio
import random
import socket
import struct
import sys

__version__ = "2.1.0"
ENCODING = "UTF-8"


class openshowvar(object):
    def __init__(self, ip, port, max_len=256, timeout=None):
        self.ip = ip
        self.port = port
        self.max_len = max_len
        self.sock = None
        self.timeout = timeout

    async def connect(self):
        loop = asyncio.get_event_loop()
        await self.close()
        self.msg_id = random.randint(1, 100)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(0)
        await loop.sock_connect(self.sock, (self.ip, self.port))

    async def close(self):
        if self.sock is not None:
            self.sock.close()
            self.sock = None

    async def read(self, var, debug=False):
        if not isinstance(var, str):
            raise Exception("Var name is a string")
        else:
            self.varname = var.encode(ENCODING)
        try:
            return await asyncio.wait_for(self._read_var(debug), self.timeout)
        except asyncio.exceptions.TimeoutError:
            raise TimeoutError(
                f"Read request for {var} exceeded timeout time {self.timeout}"
            )

    async def write(self, var, value, debug=False):
        if not (isinstance(var, str) and isinstance(value, str)):
            raise Exception("Var name and its value should be string")
        self.varname = var.encode(ENCODING)
        self.value = value.encode(ENCODING)
        try:
            return await asyncio.wait_for(self._write_var(debug), self.timeout)
        except asyncio.exceptions.TimeoutError:
            raise TimeoutError(
                f"Write request for {var} exceeded timeout time {self.timeout}"
            )

    async def _read_var(self, debug):
        req = self._pack_read_req()
        try:
            await self._send_req(req)
        except BrokenPipeError:
            await self.connect()
            req = self._pack_read_req()
            await self._send_req(req)
        _value = self._read_rsp(debug)
        return _value

    async def _write_var(self, debug):
        req = self._pack_write_req()
        try:
            await self._send_req(req)
        except BrokenPipeError:
            await self.connect()
            req = self._pack_write_req()
            await self._send_req(req)
        _value = self._read_rsp(debug)
        return _value

    async def _send_req(self, req):
        loop = asyncio.get_event_loop()
        self.rsp = None
        await loop.sock_sendall(self.sock, req)
        self.rsp = await loop.sock_recv(self.sock, self.max_len)
        # If the response is empty, it might be that the socket has failed.
        if len(self.rsp) == 0:
            raise BrokenPipeError("Connection seems to have failed. Please re-connect.")

    def _pack_read_req(self):
        var_name_len = len(self.varname)
        flag = 0
        req_len = var_name_len + 3

        return struct.pack(
            "!HHBH" + str(var_name_len) + "s",
            self.msg_id,
            req_len,
            flag,
            var_name_len,
            self.varname,
        )

    def _pack_write_req(self):
        var_name_len = len(self.varname)
        flag = 1
        value_len = len(self.value)
        req_len = var_name_len + 3 + 2 + value_len

        return struct.pack(
            "!HHBH" + str(var_name_len) + "s" + "H" + str(value_len) + "s",
            self.msg_id,
            req_len,
            flag,
            var_name_len,
            self.varname,
            value_len,
            self.value,
        )

    def _read_rsp(self, debug):
        if self.rsp is None:
            return None
        var_value_len = len(self.rsp) - struct.calcsize("!HHBH") - 3
        result = struct.unpack("!HHBH" + str(var_value_len) + "s" + "3s", self.rsp)
        _msg_id, body_len, flag, var_value_len, var_value, isok = result
        if debug:
            print("[DEBUG]", result)
        if result[-1].endswith(b"\x01") and _msg_id == self.msg_id:
            self.msg_id = (self.msg_id + 1) % 65536  # format char 'H' is 2 bytes long
            return var_value


# Run py_openshowvar as CLI after when directly executed
async def run_shell(ip, port):
    client = openshowvar(ip, port)
    await client.connect()
    print("\nConnected KRC Name: ", end=" ")
    print(await client.read("$ROBNAME[]", False))
    while True:
        data = input("\nInput var_name [, var_value]\n(`q` for quit): ")
        if data.lower() == "q":
            print("Bye")
            await client.close()
            break
        else:
            parts = data.split(",")
            if len(parts) == 1:
                print(await client.read(data.strip(), True))
            else:
                await client.write(parts[0], parts[1].lstrip(), True)


if __name__ == "__main__":
    ip = input("IP Address: ")
    port = input("Port: ")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_shell(ip, int(port)))
    loop.close()
