import socket
import struct

HOST = "127.0.0.1"  # Standard loopback interface address (localhost)
PORT = 7002  # Port to listen on (non-privileged ports are > 1023)


def parse_request(request):
    var_value_len = len(request) - struct.calcsize("!HHBH") - 3
    try:
        result = struct.unpack("!HHBH" + str(var_value_len) + "s" + "3s", request)
        return result
    except struct.error:
        return None


def start_server(storage, host=HOST, port=PORT):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((host, port))
        s.listen()
        conn, _ = s.accept()
        with conn:
            while True:
                request = conn.recv(1024)
                if not request:
                    break
                data = parse_request(request)

                if b"truevar" in request:
                    response = b"TRUE"
                elif b"falsevar" in request:
                    response = b"FALSE"
                else:
                    response = b""
                resp_len = len(response)

                if data:
                    if data[2] == 1:
                        print("Got a write request", data)
                        storage.append(data)

                    st = struct.pack(
                        "!HHBH" + str(resp_len) + "s3s",
                        data[0],
                        6,
                        0,
                        resp_len,
                        response,
                        b"\x00\x01\x01",
                    )
                else:
                    st = struct.pack(
                        "!HHBH" + str(resp_len) + "s3s",
                        48,
                        6,
                        0,
                        resp_len,
                        response,
                        b"\x00\x01\x01",
                    )
                conn.send(st)


if __name__ == "__main__":
    start_server([])
