# -*- coding: utf-8 -*-

import asyncio
import threading

import pytest
from py_openshowvar import openshowvar

from dummy_server import start_server

HOST = "127.0.0.1"
PORT = 7000

server_data = []


@pytest.fixture(scope="function", autouse=True)
def dummy_server():
    t = threading.Thread(target=start_server, args=(server_data, HOST, PORT))
    t.setDaemon(True)
    t.start()


@pytest.mark.asyncio
async def test_connect():
    client = openshowvar(HOST, PORT)
    await client.connect()
    await client.close()


@pytest.mark.asyncio
async def test_read():
    client = openshowvar(HOST, PORT)
    await client.connect()
    resp = await client.read("truevar")
    resp2 = await client.read("falsevar")
    resp3 = await client.read("nonevar")
    await client.close()
    assert resp == b"TRUE"
    assert resp2 == b"FALSE"
    assert resp3 == b""


@pytest.mark.asyncio
async def test_write():
    client = openshowvar(HOST, PORT)
    await client.connect()
    await client.write("varname", "value")
    await client.close()
    assert server_data[0][1:] == (17, 1, 7, b"varname\x00\x05va", b"lue")
