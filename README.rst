py_openshowvar
===============

Async version of the Python port of KUKA VarProxy client (OpenShowVar).
Original version on `Github <https://github.com/linuxsand/py_openshowvar>`_


Python version
===============

Supports 3.3+.


Usage
======

Module usage:

.. code-block:: python

    import py_openshowvar


    async def connect_and_read():
        client = openshowvar(HOST, PORT)
        await client.connect()
        return await client.read("$OV_PRO")


Built-in shell usage:

.. code-block:: bash

    $ python ./py_openshowvar.py
    IP Address: 192.168.19.132
    Port: 7001

    Connected KRC Name:  "xxxxxxxxxx"

    Input var_name [, var_value]
    (`q` for quit): $OV_PRO
    [DEBUG] (66, 5, 0, 2, '50', '\x00\x01\x01')
    50

    Input var_name [, var_value]
    (`q` for quit): $OV_PRO, 100
    [DEBUG] (67, 6, 1, 3, '100', '\x00\x01\x01')
    100

    Input var_name [, var_value]
    (`q` for quit): q
    Bye

